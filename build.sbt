val tuprolog = "it.unibo.alice.tuprolog" % "tuprolog" % "2.1.1"

lazy val root = (project in file(".")).settings(
  name := "pps-lab", version := "1.0", organization := "unibo.pps",
  scalaVersion := "2.11.8",
  libraryDependencies ++= Seq (tuprolog),
  scalaSource in Compile := { (baseDirectory in Compile)(_ / "src") }.value,
  scalaSource in Test := { (baseDirectory in Compile)(_ / "test") }.value
)

